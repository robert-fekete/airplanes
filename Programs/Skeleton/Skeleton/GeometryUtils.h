#ifndef  GEOMETRY_UTILS_H
#define GEOMETRY_UTILS_H

#include <vector>
#include "vec3.h"
#include "mat4.h"
#include "application_constants.h"

std::pair<float, float> operator*(float m, std::pair<float, float> coord);

std::pair<float, float> operator+(std::pair<float, float> a, std::pair<float, float> b);

std::pair<float, float> operator-(std::pair<float, float> a, std::pair<float, float> b);

float cross(std::pair<float, float> a, std::pair<float, float> b);

class GeometryUtils {

public:

    static float get_angle(vec3 a, vec3 b);

    static float get_full_range_angle(vec3 a, vec3 b);

    static vec3 get_axis(vec3 a, vec3 b);

    static mat4 get_rotation_matrix(vec3 u, float theta);

    static mat4 get_translation_matrix(vec3 u);

    static mat4 get_scaling_matrix(float scaling_factor);

    static bool has_intersect(std::pair<float, float> starting_point_a, std::pair<float, float> ending_point_a,
        std::pair<float, float> starting_point_b, std::pair<float, float> ending_point_b);

    static bool is_inside_point(std::pair<float, float> point, std::pair<float, float> infinite, std::vector<std::pair<float, float>> nodes);
};

#endif // ! GEOMETRY_UTILS_H