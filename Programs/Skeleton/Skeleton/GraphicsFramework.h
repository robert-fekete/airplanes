#ifndef GRAPHICS_FRAMEWORK_H
#define GRAPHICS_FRAMEWORK_H

#include <stdio.h>
#include <stdlib.h>
#include <functional>
#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif

#include "application_constants.h"

class GraphicsFramework
{
public:
	GraphicsFramework(int argc, char *argv[]);

	unsigned int get_shader_program_handle();
	
	void add_on_display_listener(std::function<void()> listener);

	void on_display();

	void add_on_idle_listener(std::function<void(float)> listener);

	void on_idle();

	void add_on_left_click_listener(std::function<void(int, int)> listener);

	void add_on_right_click_listener(std::function<void(int, int)> listener);

	void on_click(int button, int state, int x_coord, int y_coord);

	~GraphicsFramework();

private: 

	static char *vertex_source;
	static char *fragment_source;

	long prev_time = 0;
	unsigned int shader_program_handle;

	std::vector<std::function<void()>> on_display_listeners;
	std::vector<std::function<void(float)>> on_idle_listeners;
	std::vector<std::function<void(int, int)>> on_left_click_listeners;
	std::vector<std::function<void(int, int)>> on_right_click_listeners;

	void initialize_grapchical_environment(int argc, char *argv[]);

	void print_environment_properties();

	void on_initialization();

	unsigned int create_vertex_shader();

	unsigned int create_fragment_shader();

	void create_shader_program(unsigned int vertex_shader, unsigned int fragment_shader);

	void check_shader(unsigned int handle, char *message);

	void check_linking(unsigned int program);

	void get_error_info(unsigned int handle);

};

#endif
