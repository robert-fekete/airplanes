#include "GraphicsFramework.h"


GraphicsFramework::GraphicsFramework(int argc, char *argv[])
{
	initialize_grapchical_environment(argc, argv);
	print_environment_properties();
	on_initialization();
}

unsigned int GraphicsFramework::get_shader_program_handle(){

	return shader_program_handle;
}

void GraphicsFramework::add_on_display_listener(std::function<void()> listener){

	on_display_listeners.push_back(listener);
}

void GraphicsFramework::on_display(){

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (auto listener : on_display_listeners){
		listener();
	}

	glutSwapBuffers();
}

void GraphicsFramework::add_on_idle_listener(std::function<void(float)> listener){

	on_idle_listeners.push_back(listener);
}

void GraphicsFramework::on_idle(){

	long time = glutGet(GLUT_ELAPSED_TIME);
	long delta_time = time - prev_time;
	prev_time = time;
	float sec = delta_time / 1000.0f;

	for (auto listener : on_idle_listeners){
		listener(sec);
	}
	glutPostRedisplay();
}

void GraphicsFramework::add_on_left_click_listener(std::function<void(int, int)> listener){

	on_left_click_listeners.push_back(listener);
}

void GraphicsFramework::add_on_right_click_listener(std::function<void(int, int)> listener){

	on_right_click_listeners.push_back(listener);
}

void GraphicsFramework::on_click(int button, int state, int x_coord, int y_coord){

	if (state == GLUT_DOWN) {

		if (button == GLUT_LEFT_BUTTON) {

			for (auto listener : on_left_click_listeners){
				listener(x_coord, y_coord);
			}
		}
		else if (button == GLUT_RIGHT_BUTTON) {

			for (auto listener : on_right_click_listeners){
				listener(x_coord, y_coord);
			}
		}

		glutPostRedisplay();
	}
}

void GraphicsFramework::initialize_grapchical_environment(int argc, char *argv[]){

	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(3, 0);
#endif
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(200, 200);
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_2_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow("Szipi szupi");

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif
}

void GraphicsFramework::print_environment_properties(){

	int majorVersion, minorVersion;
	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

void GraphicsFramework::on_initialization() {

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	auto vertex_shader = create_vertex_shader();
	auto fragment_shader = create_fragment_shader();

	create_shader_program(vertex_shader, fragment_shader);

	// TODO implementing Z buffer and alpha transparency
	// Z buffer FAQ: https://www.opengl.org/archives/resources/faq/technical/depthbuffer.htm
}

unsigned int GraphicsFramework::create_vertex_shader() {


	unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	if (!vertex_shader) {
		printf("Error in vertex shader creation\n");
		exit(1);
	}
	glShaderSource(vertex_shader, 1, &vertex_source, NULL);
	glCompileShader(vertex_shader);
	check_shader(vertex_shader, "Vertex shader error");

	return vertex_shader;
}

unsigned int GraphicsFramework::create_fragment_shader() {

	unsigned int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	if (!fragment_shader) {
		printf("Error in fragment shader creation\n");
		exit(1);
	}
	glShaderSource(fragment_shader, 1, &fragment_source, NULL);
	glCompileShader(fragment_shader);
	check_shader(fragment_shader, "Fragment shader error");

	return fragment_shader;
}

void GraphicsFramework::create_shader_program(unsigned int vertex_shader, unsigned int fragment_shader){

	shader_program_handle = glCreateProgram();
	if (!shader_program_handle) {
		printf("Error in shader program creation\n");
		exit(1);
	}
	glAttachShader(shader_program_handle, vertex_shader);
	glAttachShader(shader_program_handle, fragment_shader);

	glBindAttribLocation(shader_program_handle, 0, "vertexPosition"); // vertexPosition gets values from Attrib Array 0
	glBindAttribLocation(shader_program_handle, 1, "vertexColor");    // vertexColor gets values from Attrib Array 1

	glBindFragDataLocation(shader_program_handle, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

	glLinkProgram(shader_program_handle);
	check_linking(shader_program_handle);

	glUseProgram(shader_program_handle);
}

void GraphicsFramework::check_shader(unsigned int shader, char * message) {
	int OK;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if (!OK) {
		printf("%s!\n", message);
		get_error_info(shader);
	}
}

void GraphicsFramework::check_linking(unsigned int program) {
	int OK;
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if (!OK) {
		printf("Failed to link shader program!\n");
		get_error_info(program);
	}
}

void GraphicsFramework::get_error_info(unsigned int handle) {
	int logLen;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
	if (logLen > 0) {
		char * log = new char[logLen];
		int written;
		glGetShaderInfoLog(handle, logLen, &written, log);
		printf("Shader log:\n%s", log);
		delete log;
	}
}

char *GraphicsFramework::vertex_source = R"(
		#version 130
		precision highp float;

		uniform mat4 MVP;			// Model-View-Projection matrix in row-major format

		in vec3 vertexPosition;		// variable input from Attrib Array selected by glBindAttribLocation
		in vec3 vertexColor;	    // variable input from Attrib Array selected by glBindAttribLocation
		out vec3 color;				// output attribute

		void main() {
			color = vertexColor;														// copy color from input to output
			gl_Position = vec4(vertexPosition.x, vertexPosition.y, vertexPosition.z, 1) * MVP; 		// transform to clipping space
		}
	)";

char *GraphicsFramework::fragment_source = R"(
		#version 130
		precision highp float;

		in vec3 color;				// variable input: interpolated color of vertex shader
		out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

		void main() {
			fragmentColor = vec4(color, 1); // extend RGB to RGBA
		}
	)";

GraphicsFramework::~GraphicsFramework(){

	glDeleteProgram(shader_program_handle);
}
