#include "AirPlane.h"


AirPlane::AirPlane(unsigned int shader_program_handle, Camera& camera) : CatmullRom(shader_program_handle, camera), path(AirPlanePath(shader_program_handle, camera)){

	float control_point_coordinates[12][2] = {
		{ 0.0f, -320.4f },
		{ 15.0f, -287.05f },
		{ 58.5f, -300.75f },
		{ 73.5f, -310.75f },
		{ 58.5f, -280.75f },
		{ 15.0f, -267.35f },
		{ 11.0f, -250.0f },
		{ 24.05f, -109.35f },
		{ 161.25f, -155.35f },
		{ 126.25f, -105.35f },
		{ 28.3f, -65.1f },
		{ 0.0f, 0.0f }
	};

	for (float* coordinates : control_point_coordinates) {

		add_control_point(std::make_pair(coordinates[0], coordinates[1]));
	}

	int reverse_iter = CONTROL_POINT_SIZE - 2;
	for (int i = reverse_iter; i > 0; i--) {
		float* current_coordinates = control_point_coordinates[i];

		add_control_point(std::make_pair(current_coordinates[0] * -1, current_coordinates[1]));
	}

	add_points_to_buffer();

	mat4 transformation_matrix = GeometryUtils::get_scaling_matrix(SCALING_FACTOR);
	set_own_world(transformation_matrix);
}

void AirPlane::start_segment() {
	if (path.segments.empty() || current_segment_index >= path.segments.size()) {
		is_active = false;
		path.set_to_draw(false);
		return;
	}

	is_active = true;
	PathSegment current_segment = path.segments[current_segment_index];
	start_position = current_segment.start;
	current_normal = current_segment.normal * -1;
	target_angle = GeometryUtils::get_angle(current_segment.start, current_segment.end);
	current_angle = 0;
}

void AirPlane::add_point(vec3 point) {

	path.add_point(point);
}

void AirPlane::animate(float t) {

	if (is_active) {

		current_angle += SPEED * t;
		if (current_angle > target_angle) {
			current_segment_index++;
			start_segment();
		}
	}
}

void AirPlane::draw_in_coordinate_system(mat4 new_world){

	path.draw_in_coordinate_system(new_world);
	if (is_active) {

		mat4 speed_rotation_matrix = GeometryUtils::get_rotation_matrix(current_normal, current_angle);
		vec3 position = start_position * speed_rotation_matrix;

		vec3 rotation_axis = GeometryUtils::get_axis(position, vec3(0, 0, 1));
		float rotation_angle = GeometryUtils::get_angle(position, vec3(0, 0, 1));
		mat4 rotation_matrix = GeometryUtils::get_rotation_matrix(rotation_axis, rotation_angle);
		mat4 movement_matrix = rotation_matrix;

		vec3 speed_vector = position.cross(current_normal);
		vec3 rotated_direction = DIRECTION * rotation_matrix;
		rotation_axis = GeometryUtils::get_axis(speed_vector, rotated_direction);
		rotation_angle = GeometryUtils::get_angle(speed_vector, rotated_direction);
		rotation_matrix = GeometryUtils::get_rotation_matrix(rotation_axis, rotation_angle);
		movement_matrix = movement_matrix * rotation_matrix;

		mat4 translation_matrix = GeometryUtils::get_translation_matrix(position);
		movement_matrix = movement_matrix * translation_matrix;

		Drawable3D::draw_in_coordinate_system(movement_matrix * new_world);
	}
}
