#include "Spehere.h"


Sphere::Sphere(float radius, vec3 color, unsigned int shader_program_handle, Camera& camera) :
	radius(radius),
	color(color),
	Drawable3D(shader_program_handle, camera, GL_TRIANGLE_STRIP) {

	std::vector<float> points;
	for (int iter_vertical = 1; iter_vertical < (one_dimension_precision / 2.0); iter_vertical++) {
		for (int iter_horizontal = 0; iter_horizontal < one_dimension_precision; iter_horizontal++) {

			vec3 point_a = generate_next_point(iter_vertical, iter_horizontal);
			add_point_with_color_to_vector(points, point_a, color);

			vec3 point_b = generate_next_point(iter_vertical - 1, iter_horizontal - 1);
			add_point_with_color_to_vector(points, point_b, color);

			vec3 point_c = generate_next_point(iter_vertical - 1, iter_horizontal);
			add_point_with_color_to_vector(points, point_c, color);

			vec3 point_d = generate_next_point(iter_vertical, iter_horizontal - 1);
			add_point_with_color_to_vector(points, point_d, color);
		}
	}

	add_points(points);
}


vec3 Sphere::generate_next_point(int iter_vertical, int iter_horizontal) {

	float angle = 2.0f * PI / one_dimension_precision;

	float x = cosf(iter_horizontal * angle) * sinf(iter_vertical * angle) * radius;
	float y = cosf(iter_vertical * angle) * radius;
	float z = sinf(iter_horizontal * angle) * sinf(iter_vertical * angle) * radius;

	return vec3(x, y, z);
}
