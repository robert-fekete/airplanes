#ifndef  CIRCLE_H
#define CIRCLE_H

#include "Drawable3D.h"
#include "vec3.h"
#include "application_constants.h"

class Circle : public Drawable3D {

    const vec3& color;
    int n = PRECISION;
    float radius;

public:

    Circle(float radius, vec3 color, unsigned int shader_program_handle, Camera& camera);

private:

    vec3 generate_next_point(float current_angle);
};

#endif // ! CIRCLE_H
