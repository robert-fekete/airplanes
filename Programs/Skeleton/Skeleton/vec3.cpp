#include "vec3.h"


vec3::vec3() : vec3{ 0, 0, 0 } { }

vec3::vec3(float x, float y, float z) {
    v[0] = x; v[1] = y; v[2] = z;
}

float &vec3::operator[](int i)
{
    return v[i];
}

const float &vec3::operator[](int i) const
{
    return v[i];
}

float vec3::length() {
    return sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

vec3 vec3::operator*(float scalar) {
    return vec3{ v[0] * scalar, v[1] * scalar, v[2] * scalar };
}

vec3 vec3::operator/(float scalar) {
    return vec3{ v[0] / scalar, v[1] / scalar, v[2] / scalar };
}

vec3 vec3::cross(const vec3& other) {
    return vec3{ v[1] * other[2] - v[2] * other[1], v[2] * other[0] - v[0] * other[2], v[0] * other[1] - v[1] * other[0] };
}

float vec3::dot(const vec3& other) {
    return v[0] * other[0] + v[1] * other[1] + v[2] * other[2];
}

vec3 vec3::operator*(const mat4& mat) {
    vec3 result;
    for (int j = 0; j < 3; j++) {
        result[j] = 0;
        for (int i = 0; i < 3; i++) {
            result[j] += v[i] * mat.m[i][j];
        }
    }
    return result;
}
