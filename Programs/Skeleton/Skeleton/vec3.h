#ifndef VEC3_H
#define VEC3_H

#include "mat4.h"

class vec3 {

    float v[3];

public:

    vec3();

    vec3(float x, float y, float z);

    float &operator[](int i);

    const float &operator[](int i) const;

    float length();

    vec3 operator*(float scalar);

    vec3 operator/(float scalar);

    vec3 cross(const vec3& other);

    float dot(const vec3& other);

    vec3 operator*(const mat4& mat);
};

#endif

