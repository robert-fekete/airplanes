#include "CatmullRom.h"


CatmullRom::CatmullRom(unsigned int shader_program_handle, Camera& camera) : Drawable3D(shader_program_handle, camera, GL_TRIANGLES) {
}

void CatmullRom::add_control_point(std::pair<float, float> point) {
	control_points.push_back(point);
}

void CatmullRom::add_points_to_buffer() {

	std::vector<float> vertices;
	std::vector<std::pair<float, float>> points = create_spline();


	// TODO a more optimal ear clipping algorithm http://cgm.cs.mcgill.ca/~godfried/teaching/cg-projects/97/Ian/cutting_ears.html
	while (points.size() > 3) {
		std::vector<std::pair<float, float>>::iterator iter = points.begin();
		int index = 0;

		while (iter != points.end() && points.size() > 3) {
			if (is_ear(index, points)) {

				int previous_index = get_safe_index(index, -1, points.size());
				int next_index = get_safe_index(index, 1, points.size());

				add_point_with_color_to_vector(vertices, vec3(points[previous_index].first, points[previous_index].second, 0), GREEN);
				add_point_with_color_to_vector(vertices, vec3(points[index].first, points[index].second, 0), GREEN);
				add_point_with_color_to_vector(vertices, vec3(points[next_index].first, points[next_index].second, 0), GREEN);

				iter = points.erase(iter);
			}
			else {
				iter++;
				index++;
			}
		}
	}

	add_point_with_color_to_vector(vertices, vec3(points[0].first, points[0].second, 0), GREEN);
	add_point_with_color_to_vector(vertices, vec3(points[1].first, points[1].second, 0), GREEN);
	add_point_with_color_to_vector(vertices, vec3(points[2].first, points[2].second, 0), GREEN);

	add_points(vertices);
}

std::vector<std::pair<float, float>> CatmullRom::create_spline() {

	std::vector<std::pair<float, float>> curve_points;

	if (control_points.size() < 4) {
		return curve_points;
	}

	for (unsigned int i = 0; i < control_points.size(); i++) {
		create_segment(i, curve_points);
	}
	return curve_points;
}


// A point is an ear if the diagonal between the previous and the following point is fully inside the shape
// The diagonal inside the shape if there is not intersection with any of the sides AND a random point of it
// is inside the shape
bool CatmullRom::is_ear(int index, std::vector<std::pair<float, float>> points) {

	int previous_index = get_safe_index(index, -1, points.size());
	int next_index = get_safe_index(index, 1, points.size());

	// Checking the intersection with sides
	for (unsigned int i = 0; i < points.size(); i++) {
		if (i == index || i == index + 1) {
			continue;
		}
		int next_i = get_safe_index(i, 1, points.size());
		bool intersects_side = GeometryUtils::has_intersect(points[previous_index], points[next_index], points[i], points[next_i]);
		if (intersects_side) {
			return false;
		}
	}

	float infinite_x = std::max_element(points.begin(), points.end(),
		[](std::pair<float, float> a, std::pair<float, float> b) { return a.first < b.first; })->first;
	float infinite_y = std::max_element(points.begin(), points.end(),
		[](std::pair<float, float> a, std::pair<float, float> b) { return a.second < b.second; })->second;

	// Checking if a random point on the diagonal is inside the shape
	std::pair<float, float> middle_point = 0.5 * (points[previous_index] + points[next_index]);
	std::pair<float, float> infinite = std::make_pair(infinite_x + 100, infinite_y + 100);

	return GeometryUtils::is_inside_point(middle_point, infinite, points);
}

int CatmullRom::get_safe_index(int index, int offset, int count) {

	int new_index = index + offset;
	if (new_index < 0) {
		new_index += count;
	}
	else if (new_index >= count) {
		new_index -= count;
	}
	return new_index;
}

std::vector<std::pair<float, float>> CatmullRom::create_segment(int start_index) {

	std::vector<std::pair<float, float>> curve_points;
	create_segment(start_index, curve_points);

	return curve_points;
}

void CatmullRom::create_segment(int start_index, std::vector<std::pair<float, float>>& curve_points) {

	float t[4] = { 0 };
	for (int i = 1; i < 4; i++) {
		t[i] = generate_next_t(t[i - 1], get_control_point(start_index, i - 1), get_control_point(start_index, i));
	}

	float delta = (t[2] - t[1]) / precision;
	float iter_t = t[1] + delta;
	for (int repeat_no = 1; repeat_no <= precision; repeat_no++) {

		std::pair<float, float> A1 = (t[1] - iter_t) / (t[1] - t[0]) * get_control_point(start_index, 0) + (iter_t - t[0]) / (t[1] - t[0]) * get_control_point(start_index, 1);
		std::pair<float, float> A2 = (t[2] - iter_t) / (t[2] - t[1]) * get_control_point(start_index, 1) + (iter_t - t[1]) / (t[2] - t[1]) * get_control_point(start_index, 2);
		std::pair<float, float> A3 = (t[3] - iter_t) / (t[3] - t[2]) * get_control_point(start_index, 2) + (iter_t - t[2]) / (t[3] - t[2]) * get_control_point(start_index, 3);

		std::pair<float, float> B1 = (t[2] - iter_t) / (t[2] - t[0]) * A1 + (iter_t - t[0]) / (t[2] - t[0]) * A2;
		std::pair<float, float> B2 = (t[3] - iter_t) / (t[3] - t[1]) * A2 + (iter_t - t[1]) / (t[3] - t[1]) * A3;

		std::pair<float, float> C1 = (t[2] - iter_t) / (t[2] - t[1]) * B1 + (iter_t - t[1]) / (t[2] - t[1]) * B2;

		curve_points.push_back(C1);
		iter_t += delta;
	}
}

std::pair<float, float> CatmullRom::get_control_point(int base_index, int offset) {

	int index = (base_index + offset) % control_points.size();
	return control_points[index];
}

float CatmullRom::generate_next_t(float prev_t, std::pair<float, float> prev_p, std::pair<float, float> current_p) {

	float xi = prev_p.first;
	float yi = prev_p.second;

	float xj = current_p.first;
	float yj = current_p.second;

	return pow(pow(pow(xj - xi, 2) + pow(yj - yi, 2), 0.5f), alpha) + prev_t;
}
