#ifndef PATH_SEGMENT_H
#define PATH_SEGMENT_H

#include <cmath>

#include "application_constants.h"
#include "Drawable3D.h"
#include "GeometryUtils.h"
#include "vec3.h"

class PathSegment : public Drawable3D {

	const vec3 color;
	const vec3 RELATIVE_VECTOR;
	const vec3 X_AXIS;
	float radius;

public:

	vec3 normal;
	vec3 start;
	vec3 end;

	PathSegment(vec3 start, vec3 end, const vec3& color, unsigned int shader_program_handle, Camera& camera);

private:

	mat4 calculate_rotation_matrix(vec3& normal);

	mat4 get_inverse_rotation_matrix(vec3& normal);

	int get_number_of_points(float angle);

	float get_angle_loop_increment(int number_of_points, float angle);

	float get_starting_angle(vec3 start, vec3 normal);

	vec3 generate_next_point(float current_angle);
};

#endif // !PATH_SEGMENT_H
