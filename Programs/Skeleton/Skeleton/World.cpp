#include "World.h"


World::World(unsigned int shader_program_handle, Camera& camera) : Earth(shader_program_handle, camera), shader_program_handle(shader_program_handle), camera(camera) {
	planes.push_back(AirPlane(shader_program_handle, camera));
}

void World::draw() {

	Earth::draw();

	mat4 rotation_matrix = get_rotation_matrix();
	for (auto plane : planes) {
		plane.draw_in_coordinate_system(rotation_matrix);
	}
}

void World::animate(float t) {

	Earth::animate(t);
	for (std::vector<AirPlane>::iterator plane_iter = planes.begin(); plane_iter != planes.end(); plane_iter++) {
		plane_iter->animate(t);
	}
}

void World::left_click(int x, int y){

	std::pair<float, float> coords = get_world_coordinates(x, y);

	if (is_coordinate_valid(coords)){
		add_point_to_current_path(coords);
	}
}

void World::right_click(int x, int y){

	std::pair<float, float> coords = get_world_coordinates(x, y);

	if (is_coordinate_valid(coords)){
		add_point_to_current_path(coords);
		finish_setting_current_path();
	}
}

std::pair<float, float> World::get_world_coordinates(int x, int y){

	float world_x_coord = (2.0f * x / WINDOW_WIDTH - 1) * (WORLD_SIZE / 2.0f);
	float world_y_coord = (1.0f - 2.0f * y / WINDOW_HEIGHT) * (WORLD_SIZE / 2.0f);

	return std::make_pair(world_x_coord, world_y_coord);
}

bool World::is_coordinate_valid(std::pair<float, float> coords){

	float distance_from_origo = coords.first * coords.first + coords.second * coords.second;

	return distance_from_origo < EARTH_RADIUS*EARTH_RADIUS;
}

void World::add_point_to_current_path(std::pair<float, float> coords) {

	float z = get_z_coordinate(coords.first, coords.second);
	vec3 in_world_point = vec3(coords.first, coords.second, z) * get_inverse_rotation_matrix();

	planes[current_plane_index].add_point(in_world_point);
}

void World::finish_setting_current_path() {

	planes[current_plane_index].start_segment();
	planes.push_back(AirPlane(shader_program_handle, camera));
	current_plane_index++;
}
	
float World::get_z_coordinate(float x, float y) {

	float projection_length = sqrtf(x*x + y*y);
	float z = sqrtf(EARTH_RADIUS*EARTH_RADIUS - projection_length*projection_length);

	return z;
}
