#include "Camera.h"

Camera::Camera(float world_size) {
    wCx = 0;
    wCy = 0;
    wCz = world_size / 4;
    wWx = world_size;
    wWy = world_size;
    wWz = world_size / 2;
}

mat4 Camera::V() { // view matrix: translates the center to the origin
    return mat4(1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        -wCx, -wCy, -wCz, 1);
}

mat4 Camera::P() { // projection matrix: scales it to be a square of edge length 2
    return mat4(2 / wWx, 0, 0, 0,
        0, 2 / wWy, 0, 0,
        0, 0, 2 / wWz, 0,
        0, 0, 0, 1);
}
