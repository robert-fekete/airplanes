#include "Earth.h"


Earth::Earth(unsigned int shader_program_handle, Camera& camera) : shader_program_handle(shader_program_handle), camera(camera) {

	siluette = std::make_unique<Sphere>(Sphere(EARTH_RADIUS, WHITE, shader_program_handle, camera));

	for (int i = 0; i < 6; i++) {

		add_vertical_ring(i);
	}
	for (int i = 1; i < 6; i++) {

		add_horizontal_ring(i);
	}
}

void Earth::animate(float t) {
	rotation_angle += ROTATIONAL_SPEED * t * 2 * PI;
}

void Earth::draw() {

	mat4 rotation_matrix = get_rotation_matrix();
	siluette->draw_in_coordinate_system(rotation_matrix);

	for (unsigned int i = 0; i < circles.size(); i++) {
		circles[i]->draw_in_coordinate_system(rotation_matrix);
	}
}
	
void Earth::add_vertical_ring(int number) {

	float thirty_degrees_in_rad{ float(PI) / 6.0f };

	std::unique_ptr<Circle> circle{ new Circle(EARTH_RADIUS, GRAY, shader_program_handle, camera) };

	mat4 tranformation_matrix = GeometryUtils::get_rotation_matrix(vec3(0, 1, 0), number * thirty_degrees_in_rad);
	circle->transform_own_world(tranformation_matrix);

	circles.push_back(std::move(circle));
}

void Earth::add_horizontal_ring(int number) {

	float thirty_degrees_in_rad{ float(PI) / 6.0f };

	float radius = sinf(number * thirty_degrees_in_rad) * EARTH_RADIUS;
	std::unique_ptr<Circle> circle{ new Circle(radius, GRAY, shader_program_handle, camera) };

	mat4 rotation_matrix = GeometryUtils::get_rotation_matrix(vec3(1.0f, 0.0f, 0.0f), PI / 2.0f);
	circle->transform_own_world(rotation_matrix);

	float offset = cosf(number * thirty_degrees_in_rad) * EARTH_RADIUS;
	mat4 translation_matrix = GeometryUtils::get_translation_matrix(vec3(0, offset, 0));
	circle->transform_own_world(translation_matrix);

	circles.push_back(std::move(circle));
}
	
mat4 Earth::get_rotation_matrix() {

	return GeometryUtils::get_rotation_matrix(vec3(0, 1, 0), rotation_angle);
}

mat4 Earth::get_inverse_rotation_matrix() {

	return GeometryUtils::get_rotation_matrix(vec3(0, -1, 0), rotation_angle);
}
