#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <memory>

#include "GraphicsFramework.h"
#include "World.h"

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif

std::unique_ptr<GraphicsFramework> framework;

void on_display() {
    
	framework->on_display();
}

void on_mouse(int button, int state, int x_coord, int y_coord) {

	framework->on_click(button, state, x_coord, y_coord);
}

void on_idle() {
    
	framework->on_idle();}


int main(int argc, char * argv[]) {
    
	framework = std::make_unique<GraphicsFramework>(GraphicsFramework(argc, argv));
	auto shader_program_handle = framework->get_shader_program_handle();
	World world(shader_program_handle, Camera(WORLD_SIZE));

	framework->add_on_display_listener([&world]() -> void{ world.draw(); });
	framework->add_on_idle_listener([&world](float sec) -> void{ world.animate(sec); });
	framework->add_on_left_click_listener([&world](int x, int y) -> void{ world.left_click(x, y); });
	framework->add_on_right_click_listener([&world](int x, int y) -> void{ world.right_click(x, y); });
	
	glutDisplayFunc(on_display);
	glutMouseFunc(on_mouse);
	glutIdleFunc(on_idle);

    glutMainLoop();
    return 1;
}
