#ifndef  CAMERA_H
#define CAMERA_H

#include "mat4.h"

// 2D camera
struct Camera {
    float wCx, wCy, wCz;	// center in world coordinates
    float wWx, wWy, wWz;	// width and height in world coordinates

public:
    Camera(float);

    mat4 V();

    mat4 P();
};

#endif // ! CAMERA_H
