#include "Circle.h"


Circle::Circle(float radius, vec3 color, unsigned int shader_program_handle, Camera& camera) :
    color(color),
    radius(radius),
    Drawable3D(shader_program_handle, camera, GL_LINE_LOOP) {

    float current_angle = 0;
    std::vector<float> points;
    for (int i = 0; i < n; i++) {

        vec3 next_point = generate_next_point(current_angle);
        add_point_with_color_to_vector(points, next_point, color);

        current_angle += 2.0f * PI / n;
    }

    add_points(points);
}

vec3 Circle::generate_next_point(float current_angle) {

    float x = cosf(current_angle) * radius;
    float y = sinf(current_angle) * radius;
    float z = 0;

    return vec3(x, y, z);
}