#ifndef CATMULLROM_H
#define CATMULLROM_H

#include "Drawable3D.h"
#include "GeometryUtils.h"
#include "vec3.h"

class CatmullRom : public Drawable3D {

	const vec3 GREEN = vec3(0, 1, 0);
	int precision = 8;
	float alpha = 0.5;

	std::vector<std::pair<float, float>> control_points;

public:

	CatmullRom(unsigned int shader_program_handle, Camera& camera);

	void add_control_point(std::pair<float, float> point);

	void add_points_to_buffer();

	std::vector<std::pair<float, float>> create_spline();

private:

	bool is_ear(int index, std::vector<std::pair<float, float>> points);

	int get_safe_index(int index, int offset, int count);

	std::vector<std::pair<float, float>> create_segment(int start_index);

	void create_segment(int start_index, std::vector<std::pair<float, float>>& curve_points);

	std::pair<float, float> get_control_point(int base_index, int offset);

	float generate_next_t(float prev_t, std::pair<float, float> prev_p, std::pair<float, float> current_p);
};

#endif // !CATMULLROM_H

