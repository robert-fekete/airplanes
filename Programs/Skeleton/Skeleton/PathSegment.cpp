#include "PathSegment.h"


PathSegment::PathSegment(vec3 start, vec3 end, const vec3& color, unsigned int shader_program_handle, Camera& camera)
	: color(color),
	RELATIVE_VECTOR(vec3(0, -1, 0)),
	X_AXIS(vec3(1, 0, 0)),
	radius(start.length()),
	start(start),
	end(end),
	Drawable3D(shader_program_handle, camera)
{
	vec3 cross_product = start.cross(end);
	normal = cross_product / cross_product.length();

	mat4 rotation_matrix = calculate_rotation_matrix(normal);
	set_own_world(rotation_matrix);

	float starting_angle = get_starting_angle(start, normal);

	float angle_of_vectors = GeometryUtils::get_angle(start, end);
	int number_of_points = get_number_of_points(angle_of_vectors);
	float delta = get_angle_loop_increment(number_of_points, angle_of_vectors);

	float current_angle = starting_angle;
	std::vector<float> points;
	for (int i = 0; i < number_of_points; i++) {

		vec3 next_point = generate_next_point(current_angle);
		add_point_with_color_to_vector(points, next_point, color);

		current_angle += delta;
	}

	add_points(points);
}


mat4 PathSegment::calculate_rotation_matrix(vec3& normal) {

	float rotation_angle = GeometryUtils::get_angle(normal, RELATIVE_VECTOR);
	vec3 rotation_axis = GeometryUtils::get_axis(normal, RELATIVE_VECTOR);

	mat4 rotation_matrix = GeometryUtils::get_rotation_matrix(rotation_axis, rotation_angle);
	return rotation_matrix;
}

mat4 PathSegment::get_inverse_rotation_matrix(vec3& normal) {

	float rotation_angle = GeometryUtils::get_angle(normal, RELATIVE_VECTOR);

	vec3 rotation_axis = GeometryUtils::get_axis(RELATIVE_VECTOR, normal);
	return GeometryUtils::get_rotation_matrix(rotation_axis, rotation_angle);
}

int PathSegment::get_number_of_points(float angle) {

	float circle_ratio = angle / 2 / PI;
	int number_of_circular_sectors = max((int)fabs(round(circle_ratio * PRECISION)), 1);

	int number_of_points = number_of_circular_sectors + 1;
	return number_of_points;
}

float PathSegment::get_angle_loop_increment(int number_of_points, float angle) {

	int number_of_circular_sectors = number_of_points - 1;
	float increment = angle / number_of_circular_sectors;

	return increment;
}

float PathSegment::get_starting_angle(vec3 start, vec3 normal) {

	mat4 inverse_rotation_matrix = get_inverse_rotation_matrix(normal);

	vec3 inverted_start = start * inverse_rotation_matrix;
	float starting_angle = GeometryUtils::get_full_range_angle(X_AXIS, inverted_start);

	return starting_angle;
}

vec3 PathSegment::generate_next_point(float current_angle) {

	float x = cosf(current_angle) * radius;
	float y = 0;
	float z = sinf(current_angle) * radius;

	return vec3(x, y, z);
}
