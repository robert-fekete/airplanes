#include "mat4.h"



mat4::mat4() : m({ {
    { { 0, 0, 0, 0 } },
    { { 0, 0, 0, 0 } },
    { { 0, 0, 0, 0 } },
    { { 0, 0, 0, 0 } }
    } }) {}

mat4::mat4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33) : m({ {
        { { m00, m01, m02, m03 } },
        { { m10, m11, m12, m13 } },
        { { m20, m21, m22, m23 } },
        { { m30, m31, m32, m33 } }
        } }) {}

mat4 mat4::operator*(const mat4& right) {
    mat4 result;
    for (auto i = 0; i < 4; ++i) {
        for (auto j = 0; j < 4; ++j) {

            result.m[i][j] = 0;
            for (auto k = 0; k < 4; ++k) {

                result.m[i][j] += m[i][k] * right.m[k][j];
            }
        }
    }
    return result;
}

mat4::operator float*() {
    return &m[0][0];
}

mat4 mat4::IDENTITY() {
    return mat4{ 1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1 };
}