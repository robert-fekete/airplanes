#ifndef  APPLICATION_CONSTANTS_H
#define APPLICATION_CONSTANTS_H

const float PI = 3.14159265359f;
const unsigned int WINDOW_WIDTH = 1000;
const unsigned int WINDOW_HEIGHT = 1000;
const float EARTH_RADIUS = 6366.19772368f;
const float WORLD_SIZE = 14147.1060526f;
const int PRECISION = 100;

#endif // ! APPLICATION_CONSTANTS_H
