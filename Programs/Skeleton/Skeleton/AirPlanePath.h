#ifndef AIRPLANE_PATH_H
#define AIRPLANE_PATH_H

#include <vector>

#include "Camera.h"
#include "PathSegment.h"
#include "vec3.h"

class AirPlanePath {

	const float FLIGHT_HEIGHT = 100;
	const vec3 RED = vec3(1, 0, 0);
	vec3 previous_node;
	bool initialized = false;
	bool to_draw = true;

	unsigned int shader_program_handle;
	Camera camera;

public:

	std::vector<PathSegment> segments;

	AirPlanePath(unsigned int shader_program_handle, Camera& camera);

	void add_point(float x, float y, float z);

	void add_point(vec3 point);

	void draw_in_coordinate_system(mat4 new_world);

	void set_to_draw(bool _to_draw);
};

#endif // !AIRPLANE_PATH_H
