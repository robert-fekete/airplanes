#include "Drawable3D.h"



Drawable3D::Drawable3D(unsigned int shader_program_handle, Camera camera) :
    Drawable3D(shader_program_handle, camera, GL_LINE_STRIP) {}

Drawable3D::Drawable3D(unsigned int shader_program_handle, Camera camera, GLenum draw_mode) :
    shader_program_handle(shader_program_handle),
    camera(camera),
    own_world(mat4::IDENTITY()),
    number_of_points(0),
    draw_mode(draw_mode) {

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glEnableVertexAttribArray(vertex_position_attribute);
    glVertexAttribPointer(vertex_position_attribute, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), reinterpret_cast<void*>(0));

    glEnableVertexAttribArray(vertex_color_attribute);
    glVertexAttribPointer(vertex_color_attribute, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));
}

void Drawable3D::draw() {

    draw_in_coordinate_system(mat4::IDENTITY());
}

void Drawable3D::draw_in_coordinate_system(mat4 new_world) {

    mat4 MVPTransform = own_world * new_world * camera.V() * camera.P();

    int location = glGetUniformLocation(shader_program_handle, "MVP");
    if (location >= 0) glUniformMatrix4fv(location, 1, GL_TRUE, MVPTransform);
    else printf("uniform MVP cannot be set\n");

    glBindVertexArray(vao);
    glDrawArrays(draw_mode, 0, number_of_points);
}

void Drawable3D::transform_own_world(mat4 matrix) {
    own_world = own_world * matrix;
}
    
void Drawable3D::add_points(std::vector<float> vertices) {

    if (vertices.size() == 0) {
        return;
    }

    number_of_points = vertices.size() / 6;

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
}

void Drawable3D::set_own_world(mat4 matrix) {
    own_world = matrix;
}

void Drawable3D::add_point_with_color_to_vector(std::vector<float>& vector, vec3& point, const vec3& color) {

    vector.push_back(point[0]);
    vector.push_back(point[1]);
    vector.push_back(point[2]);

    vector.push_back(color[0]);
    vector.push_back(color[1]);
    vector.push_back(color[2]);
}