#include "GeometryUtils.h"

std::pair<float, float> operator*(float m, std::pair<float, float> coord) {

    return std::make_pair(coord.first * m, coord.second * m);
}

std::pair<float, float> operator+(std::pair<float, float> a, std::pair<float, float> b) {

    return std::make_pair(a.first + b.first, a.second + b.second);
}

std::pair<float, float> operator-(std::pair<float, float> a, std::pair<float, float> b) {

    return std::make_pair(a.first - b.first, a.second - b.second);
}

float cross(std::pair<float, float> a, std::pair<float, float> b) {

    return a.first*b.second - a.second*b.first;
}

float GeometryUtils::get_angle(vec3 a, vec3 b) {

    auto cos_angle = a.dot(b) / a.length() / b.length();
    auto angle = acosf(cos_angle);
    return angle;
}

float GeometryUtils::get_full_range_angle(vec3 a, vec3 b) {

    auto angle = get_angle(a, b);
    auto is_positive_angle = a.cross(b)[1] < 0;

    auto signed_angle = is_positive_angle ? angle : -1 * angle + 2 * PI;
    return signed_angle;
}

vec3 GeometryUtils::get_axis(vec3 a, vec3 b) {

    auto axis = a.cross(b);
    auto normalized_axis = axis / axis.length();

    return normalized_axis;
}

mat4 GeometryUtils::get_rotation_matrix(vec3 u, float theta) {
    return mat4{ cosf(theta) + pow(u[0], 2) * (1 - cosf(theta)), u[0] * u[1] * (1 - cosf(theta)) - u[2] * sinf(theta), u[0] * u[2] * (1 - cosf(theta)) + u[1] * sinf(theta), 0,
        u[1] * u[0] * (1 - cosf(theta)) + u[2] * sinf(theta), cosf(theta) + pow(u[1], 2) * (1 - cosf(theta)), u[1] * u[2] * (1 - cosf(theta)) - u[0] * sinf(theta), 0,
        u[2] * u[0] * (1 - cosf(theta)) - u[1] * sinf(theta), u[2] * u[1] * (1 - cosf(theta)) + u[0] * sinf(theta), cosf(theta) + pow(u[2], 2) * (1 - cosf(theta)), 0,
        0, 0, 0, 1 };
}

mat4 GeometryUtils::get_translation_matrix(vec3 u) {
    return mat4{ 1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        u[0], u[1], u[2], 1 };
}

mat4 GeometryUtils::get_scaling_matrix(float scaling_factor) {
    return mat4{ scaling_factor, 0, 0, 0,
        0, scaling_factor, 0, 0,
        0, 0, scaling_factor, 0,
        0, 0, 0, 1 };
}

bool GeometryUtils::has_intersect(std::pair<float, float> starting_point_a, std::pair<float, float> ending_point_a,
    std::pair<float, float> starting_point_b, std::pair<float, float> ending_point_b) {

    auto offset_a = ending_point_a - starting_point_a;
    auto offset_b = ending_point_b - starting_point_b;

    auto from_a_to_b = starting_point_b - starting_point_a;
    auto offset_a_cross_offset_b = cross(offset_a, offset_b);

    if (offset_a_cross_offset_b == 0) {
        return false;
    }

    auto t = cross(from_a_to_b, offset_b) / offset_a_cross_offset_b;
    auto u = cross(from_a_to_b, offset_a) / offset_a_cross_offset_b;

    return (0 < t && t < 1) && (0 < u && u < 1);
}

bool GeometryUtils::is_inside_point(std::pair<float, float> point, std::pair<float, float> infinite, std::vector<std::pair<float, float>> nodes) {

    int intersects = 0;
    for (unsigned int i = 0; i < nodes.size(); i++) {

        int next_index = (i + 1) % nodes.size();
        if (GeometryUtils::has_intersect(point, infinite, nodes[i], nodes[next_index])) {
            intersects++;
        }
    }

    return intersects % 2 == 1;
}