#ifndef WORD_H
#define WORD_H

#include "AirPlane.h"
#include "Earth.h"
#include "Camera.h"
#include "vec3.h"

class World : public Earth {

	int current_plane_index = 0;
	std::vector<AirPlane> planes;

	Camera camera;
	unsigned int shader_program_handle;

public:
	World(unsigned int shader_program_handle, Camera& camera);

	void draw();

	void animate(float t);

	void left_click(int x, int y);

	void right_click(int x, int y);

private:

	std::pair<float, float> get_world_coordinates(int x, int y);

	bool is_coordinate_valid(std::pair<float, float> coords);

	void add_point_to_current_path(std::pair<float, float> coords);

	void finish_setting_current_path();

	float get_z_coordinate(float x, float y);
};

#endif // !WORD_H
