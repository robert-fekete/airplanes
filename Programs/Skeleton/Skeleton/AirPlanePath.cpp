#include "AirPlanePath.h"


AirPlanePath::AirPlanePath(unsigned int shader_program_handle, Camera& camera) : shader_program_handle(shader_program_handle), camera(camera) { }

void AirPlanePath::add_point(float x, float y, float z) {
	add_point(vec3(x, y, z));
}

void AirPlanePath::add_point(vec3 point) {

	point = point / EARTH_RADIUS * (EARTH_RADIUS + FLIGHT_HEIGHT);

	if (initialized) {
		segments.push_back(PathSegment(previous_node, point, RED, shader_program_handle, camera));
	}
	else {
		initialized = true;
	}
	previous_node = point;
}

void AirPlanePath::draw_in_coordinate_system(mat4 new_world) {

	if (!to_draw) {
		return;
	}
	for (auto segment : segments) {
		segment.draw_in_coordinate_system(new_world);
	}
}

void AirPlanePath::set_to_draw(bool _to_draw) {
	to_draw = _to_draw;
}