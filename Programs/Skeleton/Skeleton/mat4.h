#ifndef mat4_h
#define mat4_h

#include <array>

class mat4 {

public:

    std::array<std::array<float, 4>, 4> m;

    mat4();

    mat4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13,
        float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33);

    mat4 operator*(const mat4& right);

    operator float*();

    static mat4 IDENTITY();
};

#endif // !mat4_h

