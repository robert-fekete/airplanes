#ifndef DRAWABLE_3D_H
#define DRAWABLE_3D_H

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/freeglut.h>
#endif

#include <vector>
#include "mat4.h"
#include "vec3.h"
#include "Camera.h"

class Drawable3D {

    unsigned int shader_program_handle;

    const int vertex_position_attribute = 0;
    const int vertex_color_attribute = 1;

    GLuint vao;
    GLuint vbo;

    GLenum draw_mode;
    mat4 own_world;
    int number_of_points;
    Camera camera;

public:

    Drawable3D(unsigned int shader_program_handle, Camera camera);

    Drawable3D(unsigned int shader_program_handle, Camera camera, GLenum draw_mode);

    void draw();

    void virtual draw_in_coordinate_system(mat4 new_world);

    void transform_own_world(mat4 matrix);

protected:

    void add_points(std::vector<float> vertices);

    void set_own_world(mat4 matrix);

    void add_point_with_color_to_vector(std::vector<float>& vector, vec3& point, const vec3& color);
};

#endif