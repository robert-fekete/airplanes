#ifndef AIRPLANE_H
#define AIRPLANE_H

#include "AirPlanePath.h"
#include "CatmullRom.h"
#include "vec3.h"

class AirPlane : public CatmullRom {

public:

	bool is_active = false;

private:

	const float SCALING_FACTOR{ 4.0 };
	const int CONTROL_POINT_SIZE{ 12 };
	const float SPEED = 5000.0f / EARTH_RADIUS;
	vec3 DIRECTION = vec3(0, 1, 0);

	AirPlanePath path;
	unsigned int current_segment_index = 0;
	vec3 start_position;
	vec3 current_normal;
	float current_angle = 0;
	float target_angle = 0;


public:

	AirPlane(unsigned int shader_program_handle, Camera& camera);

	void start_segment();

	void add_point(vec3 point);

	void animate(float t);

	void draw_in_coordinate_system(mat4 new_world) override;
};

#endif // !AIRPLANE_H
