#ifndef EARTH_H
#define EARTH_H

#include <memory>
#include <vector>

#include "Camera.h"
#include "Circle.h"
#include "GeometryUtils.h"
#include "Spehere.h"
#include "vec3.h"

class Earth {
	const vec3 WHITE = vec3(0.9f, 0.9f, 0.9f);
	const vec3 GRAY = vec3(0.8f, 0.8f, 0.8f);
	const float ROTATIONAL_SPEED = -0.1f;
	std::vector<std::unique_ptr<Circle>> circles;
	std::unique_ptr<Sphere> siluette;

	float rotation_angle = 0;
	unsigned int shader_program_handle;
	Camera camera;

public:
	Earth(unsigned int shader_program_handle, Camera& camera);

	void animate(float t);

	void draw();

private:

	void add_vertical_ring(int number);

	void add_horizontal_ring(int number);

protected:

	mat4 get_rotation_matrix();

	mat4 get_inverse_rotation_matrix();
};

#endif // !EARTH_H
