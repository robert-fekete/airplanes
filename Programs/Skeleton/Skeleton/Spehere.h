#ifndef SPHERE_H
#define SPHERE_H

#include "application_constants.h"
#include "Drawable3D.h"
#include "vec3.h"

class Sphere : public Drawable3D {

	const vec3& color;
	int one_dimension_precision = PRECISION;
	float radius;

public:

	Sphere(float radius, vec3 color, unsigned int shader_program_handle, Camera& camera);

private:

	vec3 generate_next_point(int iter_vertical, int iter_horizontal);
};

#endif
